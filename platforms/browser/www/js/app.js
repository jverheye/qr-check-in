var app = {
    currentPage: null,
    init: function () {
        document.getElementById('button_back').hidden = true;
        document.getElementById('page_scan').hidden = true;
        document.getElementById('page_search').hidden = true;
        document.getElementById('page_selectEvent').hidden = true;
        document.getElementById('page_settings').hidden = true;
        document.addEventListener("backbutton", function () {
            if (!app.preventClosePageOnBack) {
                app.closePage();
            }
        }, false);
        app.scannerOptions = {
            preferFrontCamera : false,
            showFlipCameraButton : false,
            showTorchButton : false,
            torchOn: true,
            saveHistory: false,
            prompt : "Place a QR code inside the screen area",
            resultDisplayDuration: 0,
            formats : "QR_CODE",
            orientation : "portrait",
            disableAnimations : true,
            disableSuccessBeep: true
        };
        app.tryAutoSelectEvent();
    },
    renderHome: function () {
        app.renderTemplate('template_home', { selectedEvent: app.selectedEvent }, 'home_content')
    },
    tryAutoSelectEvent: function () {
        if (!app.selectedEvent && window.localStorage.getItem('serverUrl') != null) {
            app.httpCall('GET', '/api/event.list_for_check_in', function (events) {
                if (events.length === 1) {
                    app.selectedEvent = events[0];
                    app.renderHome();
                }
            });
        }
    },
    closePage: function () {
        document.getElementById('app_title').hidden = false;
        document.getElementById('nav').hidden = false;
        document.getElementById('button_back').hidden = true;
        document.getElementById('home_content').hidden = false;
        document.getElementById('page_' + app.currentPage).hidden = true;
        app.renderHome();
    },
    showPage: function(id, readyCallback) {
        app.currentPage = id;
        document.getElementById('app_title').hidden = true;
        document.getElementById('button_back').hidden = false;
        document.getElementById('nav').hidden = true;
        document.getElementById('home_content').hidden = true;
        document.getElementById('page_' + id).hidden = false;
        if (app['initPage_' + id]) {
            app['initPage_' + id]();
        }
        if (readyCallback) {
            readyCallback();
        }
    },
    initPage_settings: function () {
        document.getElementById('input_serverUrl').value = window.localStorage.getItem('serverUrl');
        document.getElementById('input_key').value = window.localStorage.getItem('apiKey');
    },
    initPage_scan: function () {
        if (!app.selectedEvent) {
            alert('Select an event first');
            app.closePage();
        }
        var showScanPage = function (eventStats) {
            app.renderTemplate('template_page_scan', { eventName: app.selectedEvent.name, eventStats: eventStats }, 'page_scan_content');
        };
        app.httpCall('GET', '/api/event.get_stats?id=' + app.selectedEvent.id, showScanPage, showScanPage);
    },
    initPage_search: function () {
        if (!app.selectedEvent) {
            alert('Select an event first');
            app.closePage();
        }
        app.renderTemplate('template_page_search', { eventName: app.selectedEvent.name }, 'page_search_content');
        var inputField = document.getElementById('input_keyword');
        inputField.onchange = function () {
            var url = '/api/ticket.search?eventId=' + app.selectedEvent.id + '&keywords=' + inputField.value;
            app.httpCall('GET', url, function (results) {
                for (var i = 0; i < results.length; i++) {
                    if (!results[i].firstName && !results[i].lastName) {
                        results[i].firstName = 'WITHOUT NAME';
                    }
                }
                console.log('search returned ' + results.length + ' results');
                app.renderTemplate('template_search_results', { results: results }, 'search_results');
            }, function (xhr) {
                alert('Error in search: ' + xhr.status);
            });
        };
    },
    initPage_selectEvent: function () {
        app.httpCall('GET', '/api/event.list_for_check_in', function (events) {
            app.renderTemplate('template_list_events', { events: events }, 'page_selectEvent_content');
        }, function () {
            alert('Failed to fetch events for check-in');
        });
    },
    renderTemplate: function (id, data, targetId) {
        var rendered = Mustache.render(document.getElementById(id).innerHTML, data);
        document.getElementById(targetId).innerHTML = rendered;
    },
    scanSettings: function () {
        app.preventClosePageOnBack = true;
        var cancelListener = function () { cordova.plugins.barcodeScanner.cancel(); };
        document.addEventListener("backbutton", cancelListener, false);
        cordova.plugins.barcodeScanner.scan(
            function (result) {
                if (!result.cancelled) {
                    var settings = JSON.parse(result.text);
                    if (settings && settings.serverUrl && settings.key) {
                        document.getElementById('input_serverUrl').value = settings.serverUrl;
                        document.getElementById('input_key').value = settings.key;
                    } else {
                        alert('Not a settings QR code');
                    }
                }
                setTimeout(function () {
                    app.preventClosePageOnBack = false;
                }, 1000);
            }, function (error) {
                alert('Scanning failed: ' + error);
                setTimeout(function () {
                    app.preventClosePageOnBack = false;
                }, 1000);
            }, app.scannerOptions);
    },
    manualCheckIn: function (ticketId, barcode) {
        if (confirm('Check in T' + ticketId + '?')) {
            app.httpCall('GET', '/api/ticket.check_in?eventId=' + app.selectedEvent.id + '&ticket=' + barcode, function (checkInResult) {
                app.closePage();
                app.showPage('scan', function() {
                    app.handleCheckInResult(checkInResult)
                });
            }, function () {
                alert('Manual check-in failed. Please try again.');
            });
        }
    },
    scanNext: function (checkInResult) {
        var openScanner = function () {
            app.renderScanPage(null);
            app.preventClosePageOnBack = true;
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    if (!result.cancelled) {
                        app.httpCall('GET', '/api/ticket.check_in?eventId=' + app.selectedEvent.id + '&ticket=' + result.text, app.handleCheckInResult, function () {
                            alert('Ticket verification failed. Try again.')
                        });
                    }
                    setTimeout(function () {
                        app.preventClosePageOnBack = false;
                    }, 1000);
                }, function (error) {
                    alert('Scanning failed: ' + error);
                    setTimeout(function () {
                        app.preventClosePageOnBack = false;
                    }, 1000);
                }, app.scannerOptions)
        };
        var cancelListener = function () { cordova.plugins.barcodeScanner.cancel(); };
        document.addEventListener("backbutton", cancelListener, false);
        openScanner();
    },
    renderScanPage: function (checkInResult, eventStats) {
        if (checkInResult) {
            if (!checkInResult.firstName && !checkInResult.lastName) {
                checkInResult.firstName = 'WITHOUT NAME';
            }
            if (checkInResult.messages && checkInResult.messages.length > 0) {
                alert('There are messages on this ticket');
            }
            if (checkInResult.status === 'ok' && (!checkInResult.messages || checkInResult.messages.length === 0)) {
                setTimeout(app.scanNext, 1000);
            }
        }
        app.renderTemplate('template_page_scan', {
            eventName: app.selectedEvent.name,
            checkInResult: checkInResult,
            showDetails: checkInResult && (checkInResult.status !== 'nok'),
            eventStats: eventStats
        }, 'page_scan_content');
    },
    handleCheckInResult: function (checkInResult) {
        var continueCheckInResult = function (eventStats) {
            if (checkInResult.alerts) {
                for (var i = 0; i < checkInResult.alerts.length; i++) {
                    alert(checkInResult.alerts[i]);
                }
            }
            app.renderScanPage(checkInResult, eventStats);
        };
        app.httpCall('GET', '/api/event.get_stats?id=' + app.selectedEvent.id, continueCheckInResult, continueCheckInResult);
    },
    selectEvent: function (eventId) {
        app.httpCall('GET', '/api/event.get?id=' + eventId, function (event) {
            app.selectedEvent = event;
            app.closePage();
        }, function () {
            alert('Failed to confirm event');
        });
    },
    saveSettings: function () {
        var serverUrl = document.getElementById('input_serverUrl').value;
        var key = document.getElementById('input_key').value;
        console.log('serverUrl='+serverUrl);
        console.log('apiKey='+key);
        window.localStorage.setItem('serverUrl', serverUrl);
        window.localStorage.setItem('apiKey', key);
        console.log('saved settings');
        app.httpCall('GET', '/api/user.get_current', function (user) {
            alert('Welcome ' + user.details.firstName + ' ' + user.details.lastName + '. Settings verified successfully.');
            app.closePage();
        }, function () {
            alert('Settings verification failure');
        });
    },
    httpCall: function (method, path, success, error) {
        document.getElementById('loading_indicator').className = '';
        console.log('creating XMLHttpRequest');
        var xhr = new XMLHttpRequest();
        var serverUrl = window.localStorage.getItem('serverUrl');
        var apiKey = window.localStorage.getItem('apiKey');
        console.log('opening XHR channel to ' + path);
        xhr.timeout = 10000;
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                document.getElementById('loading_indicator').className = 'loading_indicator_hidden';
                if (xhr.status === 200) {
                    console.log('request succeeded');
                    var resp = JSON.parse(xhr.responseText);
                    console.log('firing callback');
                    success(resp);
                } else if (xhr.status === 403) {
                    alert('Access denied');
                } else {
                    error(xhr);
                }
            }
        };
        var finalUrl = serverUrl + path + (path.indexOf('?') >= 0 ? '&' : '?') + 'apiKey=' + apiKey;
        console.log('finalUrl: ' + finalUrl);
        xhr.open('GET', finalUrl);
        xhr.send();
    }
};
document.addEventListener('deviceready', function () {
    app.init();
    document.getElementById('container').style = 'visibility: visible';
});
